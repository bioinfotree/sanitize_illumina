#!/bin/env python

import argparse
import re
#import ipdb
import os
from sys import stdin, stdout, exit

class Bin(object):
    
    def __init__(self):
        self.range = ''
        self.G_perc = float()
        self.A_perc = float()
        self.T_perc = float()
        self.C_perc = float()
        self.N_perc = float()
        self.qual_median = float()
        self.qual_lowerq = float()
        self.qual_upperq = float()

def init_and_qual(bins, fields):
    
    this_bin = Bin()
    this_bin.range = fields[0]
    this_bin.qual_median = float(fields[2])
    this_bin.qual_lowerq = float(fields[3])
    this_bin.qual_upperq = float(fields[4])
    bins.append(this_bin)
    #[bin for bin in bins if bin.range == line[]

def append_bases(bins, fields):
    
    for mybin in bins:
        if mybin.range == fields[0]:
            mybin.G_perc = float(fields[1])
            mybin.A_perc = float(fields[2])
            mybin.T_perc = float(fields[3])
            mybin.C_perc = float(fields[4])
            break

def append_Ns(bins, fields):
    for mybin in bins:
        if mybin.range == fields[0]:
            mybin.N_perc = float(fields[1])
            break

def get_duplicates(data, starter):
    parser = False
    dup ={}
    for line in data:
        start = re.match(r'%s' % starter, line)
        stop = re.match(r">>END_MODULE", line)
        if start:
            parser=True
            #print line
            continue  
        if parser and stop:
            parser = False
        if parser and not re.match(r"^#", line):
            fields = line.split("\t")
            dup[fields[0]] = float(fields[1])
    #ten_more = dup['5'] + dup['6'] + dup['7'] + dup['8'] + dup['9'] + dup['10++']
    #two_more =  five_more + dup['4'] + dup['3'] + dup['2']
    try:
	return (dup['2'], dup['10++'])
    except KeyError:
	return (dup['2'], dup['>10'])
    

def parseit(data, bins, parse_func, starter):
    parser = False
    for line in data:
        start = re.match(r'%s' % starter, line)
        stop = re.match(r">>END_MODULE", line)
        if start:
            parser=True
            #print line
            continue
        if parser and stop:
            return
        if parser and not re.match(r"^#", line):
            fields = line.split("\t")
            parse_func(bins, fields)
            
def get_filename(data):
    for line in data:
        m = re.match(r'^Filename\s+(.*)', line)
        if m:
            return os.path.basename(m.group(1))
        
def check_N_content(bins, maxn, slope_limit):
    outliers = []
    for i in range(len(bins)-2, -1, -1):        
        if bins[i + 1].N_perc == 0.0:
            bins[i + 1].N_perc = 0.000001
        slope_factor =  bins[i].N_perc / bins[i + 1].N_perc
        if slope_factor > slope_limit and bins[i].N_perc > maxn:
	    outliers.append('%s:%.2f:%.2f' %(bins[i].range,bins[i].N_perc,slope_factor))
	    
    return outliers

def single_qual_drop(bins, minq, slope_limit):
    outliers = []
    for i in range(len(bins)-2, -1, -1):        
        if bins[i + 1].qual_median == 0.0:
	    bins[i + 1].qual_median = 0.000001
	slope_factor =  bins[i].qual_median / bins[i + 1].qual_median
        if slope_factor < slope_limit and bins[i].qual_median < minq:
	    outliers.append('%s:%.2f:%.2f' %(bins[i].range, bins[i].qual_median,slope_factor))

    return outliers


def check_base_content(bins, max_base, base_check_free):
    outliers = []
    for i in range(0, len(bins) - base_check_free):
        for letter in ("A", "G", "C", "T"):
            attribute = getattr(bins[i], letter + "_perc")
            if (attribute > max_base):
		outliers.append('%i:%s:%.2f' %(i+1,letter,attribute))

    return outliers

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Flag nucleotide issues from fastqc report')
#     parser.add_argument('--lib-type', dest='lib',
#                         choices=['DNAseq', 'RNAseq', 'GBS', 'exome', 'target'],
#                         help='Provide library type')
    parser.add_argument('-m','--max-N',
			dest='maxn',
                        type=float,
			default=1.0,
                        help='Max allowed N percent per base position (default 1.0 = 1/100) (does apply when slope-factor rule is triggered)')
    parser.add_argument('-M','--slope-factor-N',
			dest='slopeN',
                        type=float,
                        default=2.0,
                        help='Walking from 3-prime to 5-prime, if the proportion of Ns increase by this factor every base/bin and max-N is not satisfied, a warning flag is triggered (default: 2.0)')
    parser.add_argument('-q','--slope-factor-Q',
			dest='slopeQ',
                        type=float,
                        default=0.75,
                        help='Walking from 3-prime to 5-prime, if the median quality drop by this factor and min-Q is not satisfied, warning flag is triggered (default: 0.75)')
    parser.add_argument('-Q','--minQ-base',
			dest='minq',
                        type=float,
                        default=30.0,
                        help='Min threshold of base quality to trigger slope-factor-Qs')
    parser.add_argument('-b','--max-base',
			dest='max_base',
                        type=float,
			default=50.0,
                        help='Max per-site base representation percent (default: 50.0 = 50/100)')
    parser.add_argument('-B','--base-check-free',
			dest='base_check_free',
                        type=int, default=1,
                        help='Avoid base content check to N bases/bins starting from 3-prime, in order to accomotade Illumina pipeline adapter trimming issue (default: 1)')
    parser.add_argument('-t','--get-trimm-site',
			dest='max_trimm_site',
                        help='If an issue for per-site base representation is flagged, return the rightmost site reporting an over-represented base if 0<SITE<=MAX_TRIMM_SITE. No other informations are shown. (default: False)')
                        
    my_args = parser.parse_args()
    

    data = [r.strip() for r in stdin.readlines()]
    
    bins = []
    flag_list = []
    parseit(data, bins, init_and_qual, ">>Per base sequence quality")
    parseit(data, bins, append_bases, ">>Per base sequence content")
    parseit(data, bins, append_Ns, ">>Per base N content")
    
    #mypath = os.path.abspath(my_args.fastqc)
    filename = get_filename(data)


    #########################################################################
    exit_state = 'PASS'
    outliers = check_base_content(bins, my_args.max_base, my_args.base_check_free)
    if outliers:
        exit_state = 'FAIL'
    
    if my_args.max_trimm_site: # if 0 < max_trimm_site <= rightmost, print the rightmost position to trim , otherwise exit without print anything
	    if outliers:
			rightmost = int(outliers[-1].split(':')[0])
			try:
				if rightmost <= int(my_args.max_trimm_site):   # my_args.max_trimm_site is a string. Test the conversion to int
					stdout.write('%i\n' %rightmost)
					exit(0)
				else: exit(0)
			except ValueError as e:
				stdout.write('[ERROR]: Integer required for --get-trimm-site flag!\n')
				exit(1)	
	    else: exit(0)
        
    #stdout.write('# filename\tbase content check\toverrepresented bases\tN content check\toverrepresented N\tsingle qual drop\tqual drop positions\tDup level 2x\tDup level 10+\n')
    stdout.write('%s\t' %filename)
    #print "\t".join(["base_content_check", exit_state, ','.join(outliers), filename ])
    stdout.write('%s\t%s\t' %(exit_state, ','.join(outliers)))
    #########################################################################
    exit_state = 'PASS'
    outliers = check_N_content(bins, my_args.maxn, my_args.slopeN)
    if outliers:
        exit_state = 'FAIL'

    #print "\t".join(['N_content_check', exit_state, ','.join(outliers), filename ])
    stdout.write('%s\t%s\t' %(exit_state, ','.join(outliers)))
    #########################################################################
    exit_state = 'PASS'
    outliers = single_qual_drop(bins, my_args.minq, my_args.slopeQ)
    if outliers:
        exit_state = 'FAIL'

    #print "\t".join(["single_qual_drop", exit_state, ','.join(outliers), filename ])
    stdout.write('%s\t%s\t' %(exit_state, ','.join(outliers)))
    #########################################################################
    duptwo, ten_more = get_duplicates(data, ">>Sequence Duplication Levels")
    #print "\t".join(["Dup level 2x", "%.2f" % (duptwo) + "%", filename ])
    stdout.write('%.2f\t' %duptwo)
    #print "\t".join(["Dup level 10+", "%.2f" % (ten_more) + "%", filename ])
    stdout.write('%.2f\t' %ten_more)
    stdout.write('\n')
    #ipdb.set_trace()