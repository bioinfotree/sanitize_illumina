# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:


MAX_TRIM_SITE ?= 10
MAXBASE ?= 50

# select all files that terminates with *.fastq.gz but does not begins with done.*
DONE_GZ = $(wildcard done.*)
SEQ_GZ_LN = $(filter-out $(DONE_GZ),$(wildcard *.fastq.gz))


FASTQC_ZIP_1 = $(SEQ_GZ_LN:.fastq.gz=_fastqc.zip)
%_fastqc.zip: %.fastq.gz
	$(call module_loader); \
	PREREQUISITE=$<; \
	TARGET=$@; \
	if [[ ("$${PREREQUISITE:0:4}" = "done" &&  ! -h $$PREREQUISITE) || (! "$${PREREQUISITE:0:4}" = "done") ]]; then \   * if the done...fastq.gz is not a link to the .fastq.gz without done, then execute fastqc, otherwise simply link the previous fastqc results in order to avoid run it again *
		fastqc \
		--extract \
		--format fastq \
		--contaminants $$PRJ_ROOT/local/src/IGA.adapters.txt \
		$$PREREQUISITE; \
	else \
		ln -sf $${TARGET##done.} $$TARGET; \
	fi


FASTQC_1 = $(FASTQC_ZIP_1:_fastqc.zip=_fastqc)
%_fastqc: %_fastqc.zip
	PREREQUISITE=$<; \
	TARGET=$@; \
	if [[ ("$${PREREQUISITE:0:4}" = "done" &&  ! -h $$PREREQUISITE) || (! "$${PREREQUISITE:0:4}" = "done") ]]; then \   * if the done..._fastqc.zip is not a link to the _fastqc.zip without done, then touch it, otherwise simply link the same .fastqc.zip results *
		touch $@; \
	else \
		ln -sf $${TARGET##done.} $$TARGET; \
	fi


ISSUES_1 = $(FASTQC_1:_fastqc=.check)

# describe the content of .check tables ("bawk -M targhet_name.check to see table description")
.META: %.check
	1	filename	exp_228_cabernet-franc_GCCAAT_L1_clean_mate_2.fastq.gz
	2	base_content_check	FAIL
	3	overrepresented_bases	1:A:99.63,2:T:99.75,3:A:99.49
	4	N_content_check	FAIL
	5	overrepresented_N	80-84:2.00:11280.62,60-64:2.00:1460.53
	6	single_qual_drop	FAIL
	7	qual_drop_positions	80-84:2.80:0.08,55-59:2.00:0.05
	8	dup_level_2x	 8.66
	9	dup_level_10	0.77

%.check: %_fastqc
	fastqc_flagger --max-base $(MAXBASE) <$</fastqc_data.txt >$@


SUMMARY_1 = $(FASTQC_1:_fastqc=.fqc.summary)

.META: %.fqc.summary
	1	Basic Statistics		PASS
	2	Per base sequence quality	PASS
	3	Per sequence quality scores	PASS
	4	Per base sequence content	FAIL
	5	Per base GC content		PASS
	6	Per sequence GC content		PASS
	7	Per base N content		WARN
	8	Sequence Length Distribution	WARN
	9	Sequence Duplication Levels	PASS
	10	Overrepresented sequences	PASS
	11	Kmer Content			FAIL



%.fqc.summary: %_fastqc
	cut -f 1,2  <$</summary.txt \
	| transpose --skip-missing \
	| sed '$$ d' \
	| sed 's/^/$<\t/g' >$@

DONE = $(addprefix done.,$(SEQ_GZ_LN))

# fastx_trimmer does not perform check of min length when parameter -f or -l are used
# also if -m parameter is provided.
# It performs check only with -t parameter is provided which argument must be not 0.
# So I avoid check of min length. If you whant to perform this use Trimmomatic
done.%.fastq.gz: %.fastq.gz %_fastqc
	POSITIONS=$$(fastqc_flagger --max-base $(MAXBASE) -t $(MAX_TRIM_SITE) <$^2/fastqc_data.txt); \   * fastqc_flagger with -t MAX_TRIM_SITE returns the rightmost position to trim if 0 <= POSITIONS <= MAX_TRIM_SITE, otherwise exit without print anything so POSITIONS will be empty *
	if [ ! -z "$$POSITIONS" ]; then \   * test if POSITIONS is NOT empty *
		echo; \
		echo "trimming $< at position: $$POSITIONS"; \
		echo; \
		$(call module_loader); \
		seqtk trimfq \
		-b $$POSITIONS \   * Trim N bp from the left-end of each read *
		$< \
		| gzip -c >$@; \
	else \
		ln -sf $< $@; \   * link file to new name *
	fi




FASTQC_ZIP_2 = $(DONE:.fastq.gz=_fastqc.zip)
FASTQC_2 = $(FASTQC_ZIP_2:_fastqc.zip=_fastqc)


ISSUES_2 = $(FASTQC_2:_fastqc=.check)

SUMMARY_2 = $(FASTQC_2:_fastqc=.fqc.summary)


STAT = $(DONE:.fastq.gz=.stat)
done.%.stat: done.%.fastq.gz
	$(call module_loader); \
	nt-fastq-stats --input $< >$@

COUNT = $(STAT:.stat=.count)
done.%.count: done.%.stat
	paste \
	<(echo $(basename $<)) \
	<(grep "Number_of_reads" $< | cut -f2) \
	<(grep "Min/Avg/Max_length" $< | cut -f 2-) \
	<(bawk 'BEGIN { A=C=G=T=N=TOT=0; } /^1/,/^\s*$$/ { \
	if ( NF == 13) { \
		A+=$$2; C+=$$3; G+=$$4; T+=$$5; N+=$$6; \
		#print $$2,$$3,$$4,$$5,$$6; \
		} \
	} \
	END { TOT=A+C+G+T+N; print TOT,A,C,G,T,N; }' <$<) \
	>$@


all.check: $(ISSUES_1)
	cat $^ >$@

done.all.check: $(ISSUES_2)
	cat $^ >$@

all.fqc.summary: $(SUMMARY_1)
	cat $^ >$@

done.all.fqc.summary: $(SUMMARY_2)
	cat $^ >$@

.META: done.all.count
	1	file_name	done.exp_244_kishmish-vatkana_CGATGT_L007_clean_mate_1
	2	num_reads	14644290
	3	min_reads_len	30
	4	mean_reads_len	77.1818
	5	max_read_len	100
	6	num_bases	1130082937
	7	A	349809450
	8	C	214704409
	9	G	212889149
	10	T	352666683
	11	N	13246

done.all.count: $(COUNT)
	cat $^ >$@


R1_multiqc_report.html: $(FASTQC_ZIP_1)
	$(call load_env); \
	READS_NUMBERS=$$(ls -1 "$$PWD"/*_001_fastqc.zip \
	| grep --only-matching '_R[0-9]_' \
	| sort \
	| uniq \
	| sed 's/_//g' \
	| tr $$'\n' ' '); \
	\
	for R in $${READS_NUMBERS}; do \
		rx_fastqc_input_path="$${PWD}/$${R}_fastqc"; \
		mkdir -p "$${rx_fastqc_input_path}"; \
		ls -1 $${PWD}/*_$${R}_001_fastqc.zip \
		| sed 's/done.//' \
		| bsort \
		| uniq \
		| xargs	-I {} cp -l {} "$${rx_fastqc_input_path}/"; \
		\
		multiqc \
		--template igats \
		--module fastqc \
		--force \
		--config /iga/scripts/pipelines/multiqc/run_cleanup_and_qc-raw.yaml \
		--filename "$${R}_multiqc_report.html" \
		--outdir $${PWD} \
		"$${rx_fastqc_input_path}" \
		>"$${PWD}/$${R}_multiqc.log" 2>&1 & \
		wait $$!; \
		rm -rf "$${rx_fastqc_input_path}"; \
	done;

R2_multiqc_report.html: R1_multiqc_report.html
	touch $@

# Use this if data are from SRA
# 1_multiqc_report.html: $(FASTQC_ZIP_1)
		# $(call load_env); \
		# READS_NUMBERS=$$(ls -1 "$$PWD"/*.erne_?_fastqc.zip \
		# | grep --only-matching '_[0-9]_' \
		# | sort \
		# | uniq \
		# | sed 's/_//g' \
		# | tr $$'\n' ' '); \
		# \
		# for R in $${READS_NUMBERS}; do \
				# rx_fastqc_input_path="$${PWD}/$${R}_fastqc"; \
				# mkdir -p "$${rx_fastqc_input_path}"; \
				# ls -1 $${PWD}/*.erne_$${R}_fastqc.zip \
				# | sed 's/done.//' \
				# | bsort \
				# | uniq \
				# | xargs -I {} cp -l {} "$${rx_fastqc_input_path}/"; \
				# \
				# multiqc \
				# --module fastqc \
				# --force \
				# --config "$$PRJ_ROOT/local/src/run_cleanup_and_qc-raw.yaml" \
				# --filename "$${R}_multiqc_report.html" \
				# --outdir $${PWD} \
				# "$${rx_fastqc_input_path}" \
				# >"$${PWD}/$${R}_multiqc.log" 2>&1 & \
				# wait $$!; \
				# rm -rf "$${rx_fastqc_input_path}"; \
		# done;

# 2_multiqc_report.html: R1_multiqc_report.html
	# touch $@

# for testing purpose
.PHONY: test
test:
	@echo $(SEQ_GZ_LN)



ALL +=  $(FASTQC_1) \
	$(FASTQC_ZIP_1) \
	R1_multiqc_report.html \
	R2_multiqc_report.html \
	$(ISSUES_1) \
	$(SUMMARY_1) \
	$(DONE) \
	$(FASTQC_2) \
	$(FASTQC_ZIP_2)

	# 1_multiqc_report.html \
	# 2_multiqc_report.html \

#	$(ISSUES_2) \
#	$(SUMMARY_2) \
#	$(STAT) \
#	$(COUNT) \
#	all.check \
#	done.all.check \
#	all.fqc.summary \
#	done.all.fqc.summary \
#	done.all.count


INTERMEDIATE += 


CLEAN += $(wildcard *.fastq.gz)
