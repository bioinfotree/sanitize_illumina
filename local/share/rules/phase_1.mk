# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>


LIBS ?=

# recall bmake
MAKE := bmake

# create directories and links files to be processed
dir_lst.mk:
	@echo $(shell printf "DIR_LST :=" >$@;)
	@echo $(foreach LIB, $(LIBS), \
	$(shell \
	mkdir -p $(LIB); \
	cd $(LIB); \
	for DIR in $($(LIB)); do \
		ln -sf $$DIR $$(basename "$${DIR%.*.*}").fastq.gz; \
	done; \
	cd ..; \
	printf " $(LIB)" >>$@; \   * add dir name to list *
	) )


include dir_lst.mk

# links makefiles and rules
link.mk: makefile
	RULES_PATH="$$PRJ_ROOT/local/share/rules/phase_1.1.mk"; \
	if [ -s $$RULES_PATH ] && [ -s $< ]; then \
		for DIR in $(DIR_LST); do \
			cd $$DIR; \
			ln -sf ../$<; \   * link makefile *
			ln -sf $$RULES_PATH rules.mk; \   * link rules *
			cd ..; \
		done; \
	fi


# execute bmake on each subdirectory
subsystem.flag: dir_lst.mk
	@echo Looking into subdirs: $(MAKE)
	for DIR in $(DIR_LST); do \
		if [ -d $$DIR ]; then \
			cd $$DIR; \
			$(MAKE) && \
			cd ..; \
		fi; \
	done; \
	touch $@

# for testing purpose
.PHONY: test
test:
	@echo $(MATES_FASTQ_GZ)


ALL +=  dir_lst.mk \
	link.mk \
	subsystem.flag


INTERMEDIATE += 


CLEAN += $(LIBS)


# add dipendence to clean targhet
clean: clean_dir

# recall bmake clean into each subdirectory
.PHONY: clean_dir
clean_dir:
	@echo cleaning up...
	for D in $(DIR_LST); do \
		if [ -d $$D ]; then \
			cd $$D; \
			$(MAKE) clean; \
			cd ..; \
		fi; \
	done
